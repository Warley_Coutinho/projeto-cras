import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Atendente from './layout/Atendente';
import Supervisor from './layout/Supervisor';
import Lista from './views/Cadastros/Beneficiario/Lista';
import Detalhes from './views/Cadastros/Beneficiario/Detalhes';
import Formulario from './views/Cadastros/Beneficiario/Formulario';
import AtendenteLista from './views/Cadastros/Atendente/AtendenteLista';
import AtendenteDetalhes from './views/Cadastros/Atendente/AtendenteDetalhes';
import AtendenteFormulario from './views/Cadastros/Atendente/AtendenteFormulario';

const Stack = createStackNavigator();


export default function App() {
  return (

      <NavigationContainer>
        <Stack.Navigator initialRouteName="UserLayout">
          
          <Stack.Screen name="Atendente" component={Atendente}  />
          <Stack.Screen name="Lista" component={Lista} />
          <Stack.Screen name="Detalhes" component={Detalhes} />
          <Stack.Screen name="Formulario" component={Formulario} />
          <Stack.Screen name="Supervisor" component={Supervisor}  />
          <Stack.Screen name="AtendenteLista" component={AtendenteLista} />
          <Stack.Screen name="AtendenteDetalhes" component={AtendenteDetalhes} />
          <Stack.Screen name="AtendenteFormulario" component={AtendenteFormulario} />
         

        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
