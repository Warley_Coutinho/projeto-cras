import React from 'react';

import { Formik } from 'formik';
import { FlatList, SafeAreaView, View, StatusBar, StyleSheet, Text, TextInput } from "react-native";
import { Input, Button } from 'react-native-elements';

import AtendenteService from "../../../services/AtendenteService";


class Formulario extends React.Component{

    constructor(props){
        super(props);

        const {atendente} = this.props.route.params;

        this.state = { 
            atendente: atendente
        } 


    }

    componentDidMount(){
    }

    handleSubmit = (values) => {
        console.log("PROCESSANDO FORMULÁRIO = ", values);

        AtendenteService.save(values)
        .then( response => {
            console.log("SUCESSO ", response)
        })
        .catch( error => console.log("ERRRO ", error))

    }

    mapStateToProps = () => {
        return {
            id: this.state.atendente.id || undefined,
            name: this.state.atendente.name || '',
            cpf: this.state.atendente.cpf || '',
            img: this.state.atendente.img || ''
            
        };
    }

    render(){
        return(
            <SafeAreaView>
                
                <Text> Cadastros Dos Atendentes.</Text>
                
                <Formik
                    initialValues={ this.mapStateToProps()}
                    onSubmit={ (values) => this.handleSubmit(values) }
                    enableReinitialize
                >

                    { ({handleChange, handleSubmit,values}) => (

                        <View>

                            <Input label="Nome" placeholder='Nome' value={values.name} onChangeText={handleChange("name")} />
                            <Input label="CPF" placeholder='CPF' value={values.cpf} onChangeText={handleChange("cpf")} />

                            <Button title="Salvar" onPress={handleSubmit} />

                        </View>

                    )}

                </Formik>

                        



            </SafeAreaView>
        )
    }

}

export default Formulario;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#000",
        borderBottomWidth: 1,
        margin: 10
    },
    textBold: {
        fontWeight: "bold"
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    }

   

});