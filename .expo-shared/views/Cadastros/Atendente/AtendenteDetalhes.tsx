import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, Alert, Pressable } from 'react-native';
import Modal from 'modal-react-native-web'; 

import { Button, Overlay, Icon  } from 'react-native-elements';

import AtendenteService from "../../../services/AtendenteService";


class Detalhes extends React.Component{
    
    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        this.state = { 
            id: id,   
            DATA: [],
            modalVisible: false
        }

        this.props.navigation.setOptions({
            title: "Detalhes Dos Atendentes",
            headerRight: () => (
                <View style={styles.container} >

                    <Button
                        type="clear"
                        icon={ <Icon name='refresh-circle-outline' size={30} type='ionicon' color='#333333' /> }
                        onPress={ () => this.onPressUpdate() }
                    />

                    <Button
                        type="clear"
                        icon={ <Icon name='trash-outline' type='ionicon' size={30} color='#333333' /> }
                        onPress={ () => this.setModalVisible() }
                    />

                </View>
            ),

        });

    }

    onPressUpdate = () => {
        this.props.navigation.navigate('Formulario',{atendente:this.state.DATA});
    }
    onPressExcluir = () => {

        AtendenteService.delete(this.state.id)
            .then( response => {
                this.setModalVisible();
                this.props.navigation.navigate('Lista');
            })
        
    }
 
    setModalVisible = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    }


    componentDidMount(){
        AtendenteService.findById(this.state.id).then( response => this.setState( {DATA: response.data }) )
    }

    render(){

        return(
            <View >
                
                <View style={styles.container} >
                    <Image style={styles.userIcon} source={{ uri: this.state.DATA.img }} />
                    <Text style={styles.userName} >{this.state.DATA.name}</Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> CPF: </Text>
                    <Text> {this.state.DATA.cpf}  </Text>
                </View>

                <Overlay ModalComponent={Modal} isVisible={this.state.modalVisible} onBackdropPress={() => this.setModalVisible()}>
                    <Text style={styles.textHeader}>Confirmação !</Text>
                    <Text>Deseja excluir o {this.state.DATA.name} ?</Text>

                    <View  style={styles.container}>
                        <Button
                            type="solid"
                            onPress={ () => this.setModalVisible() }
                            title="Cancelar"
                            buttonStyle={styles.btnSecondary}
                        />
                        <Button
                            type="solid"
                            onPress={ () => this.onPressExcluir() }
                            title="Excluir"
                            buttonStyle={styles.btnDanger}
                        />
                    </View>

                </Overlay>


            </View>
        )
    }

}

export default Detalhes;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#000",
        borderBottomWidth: 1,
        margin: 10
    },
    textBold: {
        fontWeight: "bold"
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    }

   

});