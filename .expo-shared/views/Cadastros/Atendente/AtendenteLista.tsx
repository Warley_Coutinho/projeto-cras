import React from 'react';
import { FlatList, SafeAreaView, View, StatusBar, StyleSheet, Text, TouchableOpacity } from "react-native";

import { Button, Overlay, Icon  } from 'react-native-elements';

import AtendenteService from "../../../services/AtendenteService";
import UsuarioAtendente from "../../../components/UsuarioAtendente";

class Lista extends React.Component{

    

    constructor(props){
        super(props);
        this.state = { DATA: [] }

        this.props.navigation.setOptions({
            title: "Listas Dos Atendentes",
            headerRight: () => (
                <View style={styles.container} >

                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle-outline' type='ionicon' size={30} color='#333333' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('Formulario',{atendente: {} });
    }

    componentDidMount(){
        AtendenteService.findAll().then( response => this.setState( {DATA: response.data }) )
    }

    render(){
        return(
            <SafeAreaView>
                <FlatList
                    data={this.state.DATA}
                    renderItem={AtendenteService}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default Lista;

const styles = StyleSheet.create({});