import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';

class Supervisor extends React.Component{

    constructor(props, context){
        super(props, context);
    }

    render(){
        return(
            <View>
                <Text>Tela Do Supervisor</Text>

                <Button
                    title="Lista de Atendentes"
                    onPress={() => this.props.navigation.push("AtendenteLista") }
                />

            </View>
        )
    }

}

export default Supervisor;

const styles = StyleSheet.create({});