import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';

class Atendente extends React.Component{

    constructor(props, context){
        super(props, context);
    }

    render(){
        return(
            <View>
                <Text>Tela Do Atendente</Text>

                <Button
                    title="Lista Dos Beneficiarios"
                    onPress={() => this.props.navigation.push("Lista") }
                />

            </View>
        )
    }

}

export default Atendente;

const styles = StyleSheet.create({});