import React from 'react';
import { FlatList, SafeAreaView, View, Image, StyleSheet } from "react-native";
import { Button, Icon  } from 'react-native-elements';
import BeneficiarioService from "../../../services/BeneficiarioService";
import UsuarioBeneficiario from "../../../components/UsuarioBeneficiario";

class Lista extends React.Component{

    

    constructor(props){
        super(props);
        this.state = { DATA: [] }

        this.props.navigation.setOptions ({
            
            title:  "Listas dos Beneficiarios",
            headerRight: () => (
                <View style={styles.container} >


                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('Formulario',{beneficiario: {} });
    }

    componentDidMount(){
        BeneficiarioService.findAll().then( response => this.setState( {DATA: response.data }) )
    }

    render(){
        return(
            <SafeAreaView >
                <FlatList
                    data={this.state.DATA}
                    renderItem={UsuarioBeneficiario}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default Lista;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    }
});