import React from 'react';
import { Formik } from 'formik';
import { SafeAreaView, View,  StyleSheet, Text,TextInput } from "react-native";
import { Button } from 'react-native-elements';
import AtendenteService from "../../../services/AtendenteService";



class AtendenteFormulario extends React.Component{

    constructor(props){
        super(props);

        const {atendente} = this.props.route.params;

        this.state = { 
            atendente: atendente
        } 


    }

    componentDidMount(){
    }

    handleSubmit = (values) => {
        

        AtendenteService.save(values)
        .then( response => {
            console.log("SUCESSO ", response)
        })
        .catch( error => console.log("ERRRO ", error))

    }

    mapStateToProps = () => {
        return {
            id: this.state.atendente.id || undefined,
            img: this.state.atendente.img || '',
            name: this.state.atendente.name || '',
            cpf: this.state.atendente.cpf || '',
            email: this.state.atendente.email || '',
            senha: this.state.atendente.senha || '',
            telefone: this.state.atendente.telefone || '',
            rua: this.state.atendente.rua || '',
            bairro: this.state.atendente.bairro || '',
            cidade: this.state.atendente.cidade || '',
            estado: this.state.atendente.estado || '',
            
            
        };
    }

    render(){
        return(
            <SafeAreaView>
                
                <Text style={styles.text}> Fazer Cadastror Dos Atendentes.</Text>
                
                <Formik
                    initialValues={ this.mapStateToProps()}
                    onSubmit={ (values) => this.handleSubmit(values) }
                    enableReinitialize
                >

                    { ({handleChange, handleSubmit,values}) => (

                        <View>
                       
                           
                            <TextInput  style={styles.input} label="Nome" placeholder='Nome' value={values.name} onChangeText={handleChange("name")} />
                            <TextInput  style={styles.input} label="CPF" placeholder='CPF' value={values.cpf} onChangeText={handleChange("cpf")} />
                            <TextInput  style={styles.input} label="Email" placeholder='Email' value={values.email} onChangeText={handleChange("email")} />
                            <TextInput  style={styles.input} label="Senha" placeholder='Senha' value={values.senha} onChangeText={handleChange("senha")} />
                            <TextInput  style={styles.input} label="Telefone" placeholder='Telefone' value={values.telefone} onChangeText={handleChange("telefone")} />
                            <TextInput  style={styles.input} label="Av/Rua" placeholder='Av/Rua' value={values.rua} onChangeText={handleChange("rua")} />
                            <TextInput  style={styles.input} label="Bairro" placeholder='Bairro' value={values.bairro} onChangeText={handleChange("bairro")} />
                            <TextInput  style={styles.input} label="Cidade" placeholder='Cidade' value={values.cidade} onChangeText={handleChange("cidade")} />
                            <TextInput  style={styles.input} label="Estado" placeholder='Estado' value={values.estado} onChangeText={handleChange("estado")} />
                            

                            <Button title="Salvar" onPress={handleSubmit} />

                        </View>

                    )}

                </Formik>

            </SafeAreaView>
        )
    }

}

export default AtendenteFormulario;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        borderBottomColor: "#f1ecec",
        margin: 10
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22,
    
    },
    text:{
       
        color:'#5686d6',
        fontSize: 20,
        
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    },
    input:{
        backgroundColor: '#f1ecec',
        width: '100%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10
    }

});