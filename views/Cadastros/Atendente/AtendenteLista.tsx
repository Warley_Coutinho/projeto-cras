import React from 'react';
import { FlatList, SafeAreaView, View, StyleSheet } from "react-native";
import { Button, Icon  } from 'react-native-elements';
import AtendenteService from "../../../services/AtendenteService";
import UsuarioAtendente from "../../../components/UsuarioAtendente";

class AtendenteLista extends React.Component{

    

    constructor(props){
        super(props);
        this.state = { DATA: [] }

        this.props.navigation.setOptions ({
            
            title:  "Listas dos Atendentes",
            headerRight: () => (
                <View style={styles.container} >


                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('AtendenteFormulario',{atendente: {} });
    }

    componentDidMount(){
        AtendenteService.findAll().then( response => this.setState( {DATA: response.data }) )
    }

    render(){
        return(
            <SafeAreaView >
                <FlatList
                    data={this.state.DATA}
                    renderItem={UsuarioAtendente}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default AtendenteLista;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    }
});