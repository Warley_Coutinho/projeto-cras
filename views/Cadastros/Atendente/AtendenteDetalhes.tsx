import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';
import Modal from 'modal-react-native-web'; 
import { Button, Overlay, Icon  } from 'react-native-elements';
import AtendenteService from "../../../services/AtendenteService";


class AtendenteDetalhes extends React.Component{
    
    constructor(props){
        super(props);

        const {id} = this.props.route.params;
        this.state = { 
            id: id,   
            DATA: [],
            modalVisible: false
        }

        this.props.navigation.setOptions({
            title : "Detalhes Dos Atendentes",
            headerRight: () => (
                <View style={styles.container} >

                    <Button
                        type="clear"
                        icon={ <Icon name='create' size={30} type='ionicon' color='red' /> }
                        onPress={ () => this.onPressUpdate() }
                    />

                    <Button
                        type="clear"
                        icon={ <Icon name='trash' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.setModalVisible() }
                    />

                </View>
            ),

        });

    }

    onPressUpdate = () => {
        this.props.navigation.navigate('AtendenteFormulario',{atendente:this.state.DATA});
    }
    onPressExcluir = () => {

        AtendenteService.delete(this.state.id)
            .then( response => {
                this.setModalVisible();
                this.props.navigation.navigate('AtendenteLista');
            })
        
    }
 
    setModalVisible = () => {
        this.setState({ modalVisible: !this.state.modalVisible });
    }


    componentDidMount(){
        AtendenteService.findById(this.state.id).then( response => this.setState( {DATA: response.data }) )
    }

    render(){

        return(
            <View >
                
                <View style={styles.container} >
                    <Image style={styles.userIcon} source={{ uri: this.state.DATA.img }} />
                    <Text style={styles.userName} >{this.state.DATA.name}</Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> CPF: </Text>
                    <Text> {this.state.DATA.cpf}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Email: </Text>
                    <Text> {this.state.DATA.email}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Senha: </Text>
                    <Text> {this.state.DATA.senha}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Telefone: </Text>
                    <Text> {this.state.DATA.telefone}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Av/Rua: </Text>
                    <Text> {this.state.DATA.rua}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Bairro: </Text>
                    <Text> {this.state.DATA.bairro}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Cidade: </Text>
                    <Text> {this.state.DATA.cidade}  </Text>
                </View>

                <View style={[styles.container, styles.textInfo]}>
                    <Text style={styles.textBold}> Estado: </Text>
                    <Text> {this.state.DATA.estado}  </Text>
                </View>

                <Overlay ModalComponent={Modal} isVisible={this.state.modalVisible} onBackdropPress={() => this.setModalVisible()}>
                    <Text style={styles.textHeader}>Confirmação !</Text>
                    <Text>Deseja excluir o {this.state.DATA.name} ?</Text>

                    <View  style={styles.container}>
                        <Button
                            type="solid"
                            onPress={ () => this.setModalVisible() }
                            title="Cancelar"
                            buttonStyle={styles.btnSecondary}
                        />
                        <Button
                            type="solid"
                            onPress={ () => this.onPressExcluir() }
                            title="Excluir"
                            buttonStyle={styles.btnDanger}
                        />
                    </View>

                </Overlay>


            </View>
        )
    }

}

export default AtendenteDetalhes;


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    },
    userIcon: {
        width: 80,
        height: 80,
        marginRight: 20
    },
    userName: {
        fontSize: 22,
    },
    textInfo: {
        margin:5,
        backgroundColor: '#f1ecec',
        width: '90%',
        fontSize: 17,
        borderRadius: 7,
        
        
    },
    textBold: {
        fontWeight: "bold",
        width: '30%',
        color: '#5686d6',
        fontSize: 17,
        borderRadius: 7,
        
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 22
    },

    btnDanger: {
        backgroundColor: "#dc3545",
        color: "#fff"
    },
    btnSecondary: {
        backgroundColor: "#6c757d",
        color: "#fff",
        marginRight: 20
    },
    
    

   

});

