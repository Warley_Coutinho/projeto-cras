4
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, TextInput, View, KeyboardAvoidingView,Image,TouchableOpacity,Text } from 'react-native';



class Login extends React.Component{

    constructor(props, context){
        super(props, context);
    }

    render(){
        return(
           <KeyboardAvoidingView style={styles.background}>
               <View style={styles.containerLogo}>
                  <Image
                  style={{
                      width: 130,
                      height: 155,
                  }}
                  source={require('../assets/img/Logo.png')}
                  />
               </View>

               <View style={styles.container}>
                   <TextInput
                      style={styles.input}
                      placeholder="Email"
                      autoCorrect={false}
                      onChangeText={()=> {}}
                   />

                   <TextInput
                      style={styles.input}
                      placeholder="Senha"
                      autoCorrect={false}
                      secureTextEntry
                      onChangeText={()=> {}}
                   />

                   <TouchableOpacity style={styles.btnSubmit}>
                       <Text style={styles.submitText}>Acessar</Text>
                   </TouchableOpacity>

                   <TouchableOpacity style={styles.btnSenha}>
                       <Text style={styles.SenhaText}>Esqueceu Senha</Text>
                   </TouchableOpacity>
                   
               </View>
           </KeyboardAvoidingView>
        )
    }
}

export default Login;

const styles = StyleSheet.create({
    background:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#096058'
    },
    containerLogo:{
        flex:1,
        justifyContent: 'center'
    },
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },

    input:{
        backgroundColor: '#fff',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10,
    },

    btnSubmit:{
        backgroundColor: '#5686d6',
        width: '90%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7
    },

    submitText:{
        Color: '#fff',
        fontSize: 18
    },
    btnSenha:{
        marginTop: 10,

    },

    SenhaText:{
        color:'#ffff'
    }
});